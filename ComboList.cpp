#include "ComboList.hpp"

namespace LibCombo {
ComboList::ComboList() = default;

ComboList::ComboList(const std::string &filepath) { this->extend(filepath); }

void ComboList::extend(const std::string &filepath) {
  std::ifstream file{filepath};
  if (!file.is_open()) {
    throw Exception("LibCombo: Failed to open file");
  }

  std::string buffer;
  while (getline(file, buffer)) {
    this->emplace_back(buffer);
  }
}

void ComboList::sort() noexcept {
  Operator::comboSorter sorter;
  std::ranges::sort(this->begin(), this->end(), sorter);
}

void ComboList::deldup() noexcept {
  this->sort();
  this->erase(std::unique(this->begin(), this->end()), this->end());
}

void ComboList::emailToUser() noexcept {
  std::ranges::for_each(this->cbegin(), this->cend(), [&](const Combo &combo) {
    size_t pos = combo.getUser().find('@');
    if (pos == std::string::npos) {
      return;
    }

    // Let's just ignore the fact that I implemented setter xD
    const_cast<std::string &>(combo.getUser()).erase(pos);
  });
}

void ComboList::userToEmail(std::vector<std::string> emails,
                            bool delete_user_combos) noexcept {
  auto buffer = new ComboList();
  for (const auto &combo : *this) {
    if (combo.getUser().find('@') != std::string::npos) {
      buffer->push_back(combo);
      continue;
    }

    if (!delete_user_combos) {
      buffer->push_back(combo);
    }

    for (const auto &email : emails) {
      buffer->emplace_back(combo.getUser() + "@" + email, combo.getPass());
    }
  }
  this->clear();
  *this = std::move(*buffer);
}

void ComboList::storeToHarddisk(const std::string &path) const {
  std::ofstream file{path};
  if (!file.is_open()) {
    throw Exception("LibCombo: failed to open");
  }

  std::string buffer;
  for (auto &combo : *this) {
    buffer = combo.getComboStr() + "\n";
    file.write(buffer.c_str(), static_cast<long long>(buffer.size()));
    buffer.clear();
  }
}
} // namespace LibCombo
