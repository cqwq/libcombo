#ifndef LIBCOMBO_COMBO_HPP
#define LIBCOMBO_COMBO_HPP

#include <exception>
#include <regex>
#include <stdexcept>
#include <string>

namespace LibCombo {
class Exception : public std::logic_error {
public:
  using logic_error::logic_error;
};

class Combo {
public:
  explicit Combo(const std::string &combo_str);
  Combo(std::string user, std::string password) noexcept;

  ~Combo() noexcept = default;

  [[nodiscard]] const std::string &getUser() const noexcept;
  [[nodiscard]] const std::string &getPass() const noexcept;

  [[nodiscard]] std::string getComboStr() const noexcept;

  void setUser(const std::string &user) noexcept;
  void setPass(const std::string &pass) noexcept;

  bool operator==(const Combo &c) const {
    return (this->user == c.getUser() && this->pass == c.getPass());
  };

private:
  std::string user;
  std::string pass;
};

namespace Operator {
using comboSorter = struct comboSorter {
  bool operator()(const Combo &a, const Combo &b) const {
    const auto a_str = a.getComboStr();
    const auto b_str = b.getComboStr();
    if (a_str < b_str) {
      return true;
    }
    return false;
  }
};

} // namespace Operator
} // namespace LibCombo

#endif // LIBCOMBO_COMBO_HPP
