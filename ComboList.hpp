#ifndef COMBOLIST_HPP
#define COMBOLIST_HPP

#include <algorithm>
#include <fstream>
#include <ranges>
#include <string>
#include <thread>
#include <vector>

#include "Combo.hpp"

namespace LibCombo {
class ComboList : public std::vector<Combo> {
public:
  ComboList();
  /* basically just calls extend */
  explicit ComboList(const std::string &filepath);

  /* Append multiple combos from a file */
  void extend(const std::string &filepath);

  /* sort combos */
  void sort() noexcept;

  /* calls sort and delete duplicate combos */
  void deldup() noexcept;

  /* strip the email part from a combo.
   * For example: test@mail.com:pass123 -> test:pass123
   */
  void emailToUser() noexcept;

  void userToEmail(std::vector<std::string> emails,
                   bool delete_user_combos = true) noexcept;

  void storeToHarddisk(const std::string &path) const;
};
} // namespace LibCombo

#endif // COMBOLIST_HPP
