#include "Combo.hpp"

namespace LibCombo {

Combo::Combo(const std::string &combo_str) {
  static const std::regex REGEX("(.*):(.*)");
  static std::smatch REGEX_MATCH;

  std::regex_search(combo_str, REGEX_MATCH, REGEX);
  if (REGEX_MATCH.size() != 3) {
    return;
  }

  this->user = REGEX_MATCH[1].str();
  this->pass = REGEX_MATCH[2].str();
}
Combo::Combo(std::string user, std::string pass) noexcept
    : user(std::move(user)), pass(std::move(pass)) {}

const std::string &Combo::getUser() const noexcept { return this->user; }
const std::string &Combo::getPass() const noexcept { return this->pass; }

std::string Combo::getComboStr() const noexcept {
  return this->user + ":" + this->pass;
}

void Combo::setUser(const std::string &user) noexcept { this->user = user; }
void Combo::setPass(const std::string &pass) noexcept { this->pass = pass; }
} // namespace LibCombo
